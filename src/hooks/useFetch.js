import { useState, useEffect, useCallback } from 'react';

const useFetch = (url, options) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const refetch = useCallback(async () => {
        setIsLoading(true);

        try {
            const response = await fetch(url, {
                method: options?.method || 'GET',
                headers: options?.headers || {},
                body: options?.method === 'POST' ? JSON.stringify(options?.params) : null,
            });

            if (!response.ok) {
                throw new Error(response.statusText);
            }

            const result = await response.json();
            setData(result);
        } catch (event) {
            setError(event);
        } finally {
            setIsLoading(false);
        }
    }, [url, options]);

    useEffect(() => {
        refetch();
    }, [refetch]);

    return { data, error, isLoading, refetch };
};

export default useFetch;