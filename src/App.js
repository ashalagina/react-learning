import { useReducer } from 'react';
import { usersReducer, UsersContext } from './redux/reducer';
import { UsersList } from "./components/UsersList";

function App() {
  const [state, dispatch] = useReducer(usersReducer, [])

  return (
      <UsersContext.Provider value={{ dispatch, state }}>
          <UsersList />
      </UsersContext.Provider>
  );
}

export default App;
