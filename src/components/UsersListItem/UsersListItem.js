import { REMOVE_USER } from '../../redux/action';

export const UsersListItem = ({ user, dispatch, onOpenModal }) => {
    const { id, name, email } = user;

    return (
        <li className="flex items-center justify-between px-3 py-1 border border-gray-200">
            <div>
                <p className="text-md text-current">{ name }</p>
                <p className="text-xs text-slate-500">{ email }</p>
            </div>
            <div className="flex items-center gap-2">
                <button
                    onClick={ () => onOpenModal(user) }
                    className="text-white bg-gray-400 rounded-md p-1 text-xs min-w-28"
                >
                    Редактировать
                </button>
                <button
                    onClick={ () => dispatch({ type: REMOVE_USER, payload: { id } }) }
                    className="text-white bg-red-600 rounded-md p-1 text-xs min-w-28"
                >
                    Удалить
                </button>
            </div>
        </li>
    );
}