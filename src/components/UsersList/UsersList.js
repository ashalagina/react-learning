import {useContext, useEffect, useState} from 'react';
import { UsersContext } from '../../redux/reducer';
import { UsersListItem } from '../UsersListItem';
import { Modal } from "../Modal";
import { UserForm } from "../UserForm";
import useFetch from "../../hooks/useFetch";

const testFetchAddress = `https://fakestoreapi.com/products?limit=8`;

export const UsersList = () => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [user, setUser] = useState(null);
    const { state: users, dispatch } = useContext(UsersContext);
    const { data } = useFetch(testFetchAddress);

   useEffect(() => {
       console.log(data);
   }, [data]);

    return (
        <div className="max-w-xl p-4">
            {!!users.length && (
                <>
                    <h1 className='font-bold text-xl mb-4'>Список пользователей</h1>

                    <ul className="grid gap-2 mb-8">
                        {users.map((user) => (
                            <UsersListItem
                                user={user}
                                dispatch={dispatch}
                                key={ user.id }
                                onOpenModal={ (user) => {
                                    setIsModalOpen(true);
                                    setUser(user);
                                } }
                            />
                        ))}
                    </ul>
                </>
            )}

            <button
                className='rounded-md bg-black px-3 py-2 text-sm font-semibold text-white'
                onClick={() => {
                    setIsModalOpen(true);
                    setUser(null);
                }}
            >
                Добавить пользователя
            </button>

            <Modal
                isOpen={isModalOpen}
                onClose={() => setIsModalOpen(false)}
            >
                <UserForm
                    user={ user }
                    onSubmit={ () => setIsModalOpen(false) }
                />
            </Modal>
        </div>
    );
}