import { useContext, useState } from 'react';
import { UsersContext } from '../../redux/reducer';
import { ADD_USER, UPDATE_USER } from '../../redux/action';

export const UserForm = ( { user, onSubmit } ) => {
    const { dispatch } = useContext(UsersContext);
    const [name, setName] = useState(user ? user.name : '');
    const [email, setEmail] = useState(user ? user.email : '');

    const onFormSubmit = (event) => {
        event.preventDefault();

        dispatch({ type: user ? UPDATE_USER : ADD_USER,
            payload: {
                id: user ? user.id : Date.now(),
                name,
                email
            } });

        onSubmit();
    }

    return (
        <form onSubmit={ onFormSubmit }>
            <div className="mb-4">
                <label htmlFor="name" className="block text-sm font-medium leading-6 text-gray-900">
                    Имя
                </label>
                <div className="mt-1">
                    <input
                        id="name"
                        name="name"
                        type="text"
                        autoComplete="given-name"
                        required
                        value={ name }
                        onChange={ (event) =>  setName(event.target.value) }
                        className="block w-full rounded-md border-0 p-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-1 focus:ring-inset focus:ring-black sm:text-sm sm:leading-6"/>
                </div>
            </div>
            <div className="mb-4">
                <label htmlFor="email" className="block text-sm font-medium leading-6 text-gray-900">
                    Email
                </label>
                <div className="mt-1">
                    <input
                        id="email"
                        name="email"
                        type="email"
                        autoComplete="email"
                        required
                        onChange={ (event) =>  setEmail(event.target.value) }
                        value={ email }
                        className="block w-full rounded-md border-0 p-2 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-1 focus:ring-inset focus:ring-black sm:text-sm sm:leading-6"/>
                </div>
            </div>
            <button type="submit"
                    className="rounded-md bg-black px-3 py-2 text-sm font-semibold text-white">
                Сохранить
            </button>
        </form>
    );
}