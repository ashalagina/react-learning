import ReactDOM from 'react-dom';

export const Modal = ({
  children,
  isOpen,
  onClose,
  classNameContent,
  classNameOverlay
}) => {
    if (!isOpen) return null;

    return ReactDOM.createPortal(
        <div
            onClick={ onClose }
            className={`fixed inset-0 flex items-center justify-center bg-black/[.3] w-full ${classNameOverlay}`}
        >
            <div
                onClick={event => event.stopPropagation()}
                className={ `bg-white p-10 rounded md:w-1/2 max-h-full max-w-lg overflow-hidden relative ${classNameContent}` }
            >
                <button className='absolute top-1 right-2 text-2xl' onClick={ onClose }>×</button>
                { children }
            </div>
        </div>,
        document.body
    )
}

