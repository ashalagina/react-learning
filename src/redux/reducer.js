import React from 'react';
import { ADD_USER, UPDATE_USER, REMOVE_USER } from './action';
export const UsersContext = React.createContext(null);

export const usersReducer = (state, action) => {
    switch (action.type) {
        case ADD_USER:
            return [...state, action.payload];
        case REMOVE_USER:
            return state.filter((user) => user.id !== action.payload.id)
        case UPDATE_USER:
            return state.map(user =>
                user.id === action.payload.id ? { ...user, ...action.payload } : user
            )
        default:
            return state
    }
}